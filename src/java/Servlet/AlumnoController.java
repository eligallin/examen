/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Model.Alumno;
import Persistence.AlumnoDAO;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 *
 * @author usuario
 */
public class AlumnoController extends BaseController {
    
    public void index() {
        LOG.info("AlumnoController->index()");
        AlumnoDAO alumnoDAO;     
        
        //objeto persistencia
        alumnoDAO = new AlumnoDAO();
        ArrayList<Alumno> alumnos = null;

        //leer datos de la persistencia
        synchronized (alumnoDAO) {
            alumnos = alumnoDAO.getAll();
        }
        request.setAttribute("alumnoList", alumnos);
        String name = "index";
        LOG.info("En AlumnoController->" + name);
//        LOG.info("Studies->" + studies.size());
        dispatch("/WEB-INF/alumno/index.jsp");        
    }
    
    public void show(String idString) throws SQLException {
        LOG.info("AlumnoController->show(" + idString +")");
        Alumno alumno;
        //objeto persistencia
        AlumnoDAO alumnoDAO;             
        alumnoDAO = new AlumnoDAO();
        alumno = alumnoDAO.get(Integer.parseInt(idString));        
        request.setAttribute("alumno", alumno);
        dispatch("/WEB-INF/alumno/show.jsp");        
    }
    
    public void register() {
        dispatch("/WEB-INF/alumno/create.jsp");        
        LOG.info("AlumnoController->create");
    }
    public void store() throws SQLException {
        //crear objeto
        Alumno alumno = loadFromRequest();

//        Book book = new Book();
//        book.setTitle(request.getParameter("title"));
//        book.setPublisher(request.getParameter("publisher"));
//        book.setPages(Integer.parseInt(request.getParameter("pages")));
//        book.setYear(Integer.parseInt(request.getParameter("year")));
        
        //objeto persistencia
        AlumnoDAO alumnoDAO;             
        alumnoDAO = new AlumnoDAO();
        
        //guardar objeto
        alumnoDAO.insert(alumno);
        LOG.info("AlumnoController->create");
        
        //redirigir
        redirect(contextPath + "/alumno/index");
        
    }
    
    public void edit(String idString) throws SQLException {
        LOG.info("idString");
        long id = toId(idString);
        AlumnoDAO  alumnoDAO = new AlumnoDAO();
        Alumno alumno = alumnoDAO.get((int) id);
        request.setAttribute("alumno", alumno);
        dispatch("/WEB-INF/alumno/edit.jsp");

    }

    public void update() throws IOException, SQLException {
        AlumnoDAO  alumnoDAO = new AlumnoDAO();
        Alumno alumno = loadFromRequest();
        //crear objeto
        alumno.setId(Integer.parseInt(request.getParameter("id")));
        
        alumnoDAO.update(alumno);
        response.sendRedirect(contextPath + "/alumno/index");
        return;
    }
    
    private Alumno loadFromRequest()
    {
        Alumno alumno = new Alumno();
        alumno.setName(request.getParameter("name"));
        alumno.setEmail(request.getParameter("email"));
        alumno.setPassword(request.getParameter("password"));
        alumno.setAge(Integer.parseInt(request.getParameter("age")));
        return alumno;
    }
            
 public void delete(String idString) throws SQLException {
        LOG.info("BookController->index()");
        //objeto persistencia
        AlumnoDAO alumnoDAO;             
        alumnoDAO = new AlumnoDAO();
        alumnoDAO.delete(Integer.parseInt(idString));
        //redirigir
        redirect(contextPath + "/alumno/index");

    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
