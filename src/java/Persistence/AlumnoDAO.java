/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistence;

import Model.Alumno;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author usuario
 */
public class AlumnoDAO  extends BaseDAO{
    public ArrayList<Alumno> getAll(){
        PreparedStatement stmt = null;
        ArrayList<Alumno> alumnos = null;

        try {
            this.connect();
            stmt = connection.prepareStatement("select * from users2");
            ResultSet rs = stmt.executeQuery();
            alumnos = new ArrayList();

            int i = 0;
            while (rs.next()) {
                i++;
                Alumno alumno = new Alumno();
                alumno.setId(rs.getInt("id"));
                alumno.setName(rs.getString("name"));
                alumno.setEmail(rs.getString("email"));
                alumno.setEmail(rs.getString("email"));
                alumno.setPassword  (rs.getString("password"));
                alumno.setAge(rs.getInt("age"));

                alumnos.add(alumno);
                LOG.info("Registro fila: " + i);
            }
            this.disconnect();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return alumnos;
        
    }
    
     public Alumno get(int id) throws SQLException {
        LOG.info("get(id)");
        Alumno alumno = new Alumno();
        this.connect();
        PreparedStatement stmt = connection.prepareStatement(
                "SELECT * FROM users2"
                + " WHERE id = ?"
        );
        stmt.setLong(1, id);
        ResultSet rs = stmt.executeQuery();
        LOG.info("consulta hecha");
        if (rs.next()) {
            LOG.info("Datos ...");
            alumno.setId(rs.getInt("id"));
            alumno.setName(rs.getString("name"));
            alumno.setEmail(rs.getString("email"));
              alumno.setPassword(rs.getString("password"));
            alumno.setAge(rs.getInt("age"));
            
            LOG.info("Datos cargados");
        } else {
            LOG.log(Level.INFO, "No hay datos para el id {0}", id);
        }
        this.disconnect();
        return alumno;
    }
     
    public void insert(Alumno alumno) throws SQLException {
        PreparedStatement stmt = null;
        this.connect();
        stmt = connection.prepareStatement(
                "INSERT INTO users2(name, email, password, age)"
                + " VALUES(?, ?, ?, ?)"
        );
        stmt.setString(1, alumno.getName());
        stmt.setString(2, alumno.getEmail());
        stmt.setString(3, alumno.getPassword());
        stmt.setInt(4, alumno.getAge());

        stmt.execute();
        this.disconnect();
    }
    
    
     public void update(Alumno alumno) throws SQLException {
        this.connect();
        PreparedStatement stmt = connection.prepareStatement(
                "UPDATE users2 SET "
                        + "name = ?, "
                        + "email = ?, "
                        + "password = ?, "
                        + "age = ? "
                        + " WHERE id = ? "
        );
        stmt.setString(1, alumno.getName());
        stmt.setString(2, alumno.getEmail());
        stmt.setString(3, alumno.getPassword());
        stmt.setInt(4, alumno.getAge());
        stmt.setLong(5, alumno.getId());
        LOG.info("id : " + alumno.getId());
        stmt.execute();
        this.disconnect();
        return;
    }
     
     public void delete(int id) throws SQLException {
        PreparedStatement stmt = null;
        this.connect();
        stmt = connection.prepareStatement(
                "DELETE FROM users2"
                + " WHERE id = ?"
        );
        stmt.setInt(1, id);
        stmt.execute();
        this.disconnect();
    }    
    
    
    
}