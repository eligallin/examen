/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  usuario
 * Created: 05-jun-2017
 */

CREATE TABLE books (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255),
    publisher VARCHAR(255),
    year int,
    pages int
);

INSERT INTO books VALUES(1, 'El Quijote', 'Anaya', 2010, 888);