<%-- 
    Document   : create
    Created on : 12-jun-2017, 18:21:23
    Author     : usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Alta de libro</h1>
        <form method="POST" action="<%= request.getContextPath()%>/book/store">
            <label>Título</label><input type="text" name="title"><br>
            <label>Editorial</label><input type="text" name="publisher"><br>
            <label>Año</label><input type="text" name="year"><br>
            <label>Páginas</label><input type="text" name="pages"><br>
            <input type="submit" value="Nuevo">
        </form>

    </body>
</html>
