<%-- 
    Document   : book
    Created on : 31-may-2017, 20:49:38
    Author     : alumno
--%>

<%@page import="java.util.Iterator"%>
<%@page import="Model.Book"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:useBean id="book" scope="request" class="Book" />
        <jsp:useBean id="bookList" scope="session" class="java.util.ArrayList" />
        <jsp:useBean id="user" scope="session" class="String" />
    </head>
    <body>
        <h4>Usuario: <%= user %></h4>
        <h1>Mi libro es:</h1>
        <ul>        
            <li>Título: <%= book.getTitle() %></li>
            <li>Editorial: <%= book.getPublisher()%></li>
            <li>Páginas: <%= book.getPages()%></li>
            <li>Año: <%= book.getYear() %></li>
        </ul>
        
        <h3>En sesión hemos guardado
            <%= bookList.size() %>
            libros</h3>
            
            
    <table>
        <tr>
            <th>Id</th>
            <th>Titulo</th>
            <th>Editorial</th>
            <th>Año</th>
            <th>Páginas</th>
        </tr>
        <% 
            Iterator<Model.Book> iterator = bookList.iterator();
            while (iterator.hasNext()) {
                Book item = iterator.next();%>
        <tr>
            <td><%= item.getTitle() %></td>
            <td><%= item.getPublisher()%></td>
            <td><%= item.getYear()%></td>
            <td><%= item.getPages()%></td>
        </tr>
        <%
            }
        %>          
    </table>            
    </body>
</html>
