<%-- 
    Document   : show
    Created on : 12-jun-2017, 18:53:08
    Author     : usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Model.Alumno"%>
<jsp:useBean id="alumno" scope="request" class="Alumno" />

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Alumno</title>
    </head>
    <body>
        <h1>Detalle del alumno:</h1>
        <ul>
            <li>ID:  <%= alumno.getId() %></li>
            <li>Name:  <%= alumno.getName() %></li>
             <li>Email:  <%= alumno.getEmail()%></li>
            <li>Password: <%= alumno.getPassword()%></li>
            <li>Age:  <%= alumno.getAge()%></li>
        </ul>
    </body>
</html>
