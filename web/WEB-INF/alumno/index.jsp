<%-- 
    Document   : index
    Created on : 05-jun-2017, 19:52:38
    Author     : usuario
--%>

<%@page import="java.util.Iterator"%>
<%@page import="Model.Alumno"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Alumno</title>
        <jsp:useBean id="alumnoList" scope="request" class="java.util.ArrayList" />

    </head>
    <body>
        <h1>Lista de alumnos</h1>
        <p><a href="<%= request.getContextPath()%>/alumno/register">Nuevo</a></p>

        <table>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Password</th>
                <th>Age</th>
                <th>Acciones</th>
            </tr>
            <%
                Iterator<Model.Alumno> iterator = alumnoList.iterator();
                while (iterator.hasNext()) {
                    Alumno item = iterator.next();%>
            <tr>
                <td><%= item.getId()%></td>
                <td><%= item.getName()%></td>
                <td><%= item.getEmail()%></td>
                <td><%= item.getPassword()%></td>
                <td><%= item.getAge()%></td>
                <td>
                    <a href="<%= request.getContextPath()%>/alumno/show/<%= item.getId() %>">Ver</a> 
                    <a href="<%= request.getContextPath()%>/alumno/delete/<%= item.getId() %>">Borrar</a> 
                    <a href="<%= request.getContextPath()%>/alumno/edit/<%= item.getId() %>">Editar</a> 
                </td>
            </tr>
            <%
                }
            %>        
        </table>

    </body>
</html>
