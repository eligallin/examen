<%-- 
    Document   : create
    Created on : 12-jun-2017, 18:21:23
    Author     : usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Model.Alumno"%>
<jsp:useBean id="alumno" scope="request" class="Alumno" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Alumno</title>
    </head>
    <body>
        <h1>Editar Alumno</h1>
        <form method="POST" action="<%= request.getContextPath()%>/alumno/update">
            <input type="hidden" name="id" value="<%= alumno.getId()%>">
            <label>Name</label><input type="text" name="name" value="<%= alumno.getName() %>"><br>
            <label>Email</label><input type="text" name="email" value="<%= alumno.getEmail()%>"><br>
            <label>Password</label><input type="text" name="password" value="<%= alumno.getPassword()%>"><br>
            <label>Age</label><input type="text" name="age" value="<%= alumno.getAge()%>"><br>
            <input type="submit" value="Guardar">
        </form>

    </body>
</html>
